import React from 'react';
import {url} from '../axios/homeApi';

const UserCard = (props) => {

    const image = `${url}/uploads/${props.photo}`;
    
    return (
        <div>
            <img src={image} alt=""/>
            <h4>{props.username}</h4>
            <h5> {`${props.firstname} ${props.lastname}`} </h5>
            <h6>{props.profession}</h6>
        </div>
    )
}

export default UserCard
