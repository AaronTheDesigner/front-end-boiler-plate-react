import React from 'react';
import {url} from '../axios/homeApi'

const LargeCard = (props) => {

    const image = `${url}/uploads/${props.photo}`;
    
    return (
        <div>
            <img src={image} alt=""/>
            <h3>{props.name}</h3>
            <h4>{props.intro}</h4>
            <p>{props.description}</p>
            <small>{props.client}</small>
        </div>
    )
}

export default LargeCard
