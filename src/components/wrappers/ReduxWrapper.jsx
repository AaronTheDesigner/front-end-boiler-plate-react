import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setRole, setLogin } from '../redux/actions/index';


const Redux = () => {
    const state = useSelector(state => state)
    const dispatch = useDispatch();

    return (
        <div>
            <h1>Redux</h1>
            <button onClick={() => {
                dispatch(setRole({ role: 'user' }))
            }}>User</button>
            <button onClick={() => {
                dispatch(setRole({ role: 'customer' }))
            }}>Customer</button>
            <button onClick={() => {
                dispatch(setRole({ role: 'contributor' }))
            }}>Contributor</button>
            <button onClick={() => {
                dispatch(setRole({ role: 'admin' }))
            }}>Administrator</button>
            <button onClick={() => {
                dispatch(setLogin({ loggedIn: true }))
            }}>Login</button>
            <button onClick={() => {
                dispatch(setLogin({ loggedIn: false }))
            }}>Logout</button>
        </div>
    )
}

export default Redux;
