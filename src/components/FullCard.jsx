import React from 'react';
import {url} from '../axios/homeApi';

const FullCard = (props) => {

    const image = `${url}/uploads/${props.photo}`;

    const createMarkup = () => {
        return {__html: props.content}
    }

    const set = () => {
        return <div dangerouslySetInnerHTML={createMarkup()} />
    }
    
    return (
        <div className="card full" >
            <div className="card-header full">
                <img src={image} alt=""/>
            </div>
            <div className="card-span full">
                <span className="tag">Tag</span>
                <h4>{props.title}</h4>
                <h5>{props.subtitle}</h5>
                <p>{props.preview}</p>
            </div>
            <div className="card-body full">
                    {set()}               
            </div>
            <div className="card-footer full">
                <span>
                    {props.createdAt}
                </span>
            </div>
        </div>
    )
}

export default FullCard
