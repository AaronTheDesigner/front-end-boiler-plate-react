import React from 'react'
import {url} from '../axios/homeApi';

const MediumCard = (props) => {

    const image = `${url}/uploads/${props.photo}`;

    return (
        <div className="card medium" >
            <div className="card-header medium">
                <img src={image} alt=""/>
            </div>
            <div className="card-body medium">
                <span className="tag">Tag</span>
                <h4>{props.title}</h4>
                <h5>{props.subtitle}</h5>
                <p>{props.preview}</p>
                <div className="user medium">
                    <img src="" alt=""/>
                    <div className="user-info medium">
                        <h5>User Name</h5>
                        <small>Created At</small>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MediumCard
