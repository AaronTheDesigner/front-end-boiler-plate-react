import React from 'react';
import { useSelector } from 'react-redux';
import PublicNav from './PublicNav';
import UserNav from './UserNav';
import CustNav from './CustNav';
import ContribNav from './ContribNav';
import AdminNav from './AdminNav';
import Auth from '../Auth';

const NavContainer = () => {
    const permissions = useSelector(state => state.permissions.role)

    const renderNav = () => {
        if (permissions === 'user') {
            return <UserNav />
        } else if (permissions === 'customer') {
            return <CustNav />
        } else if (permissions === 'contributor') {
            return <ContribNav />
        } else if (permissions === 'admin') {
            return <AdminNav />
        }

        return <PublicNav />

    }

    return (
        <div>
            <Auth />
            {renderNav()}
        </div>
    )

}

export default NavContainer;