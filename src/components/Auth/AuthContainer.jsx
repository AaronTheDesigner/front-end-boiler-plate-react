import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setRole, setLogin } from '../../redux/actions';
import { userDetails } from '../../axios/homeApi'
import LoginForm from './LoginForm';
import LogoutButton from './LogoutButton'


const AuthContainer = () => {
    const status = useSelector(state => state.status.loggedIn)
    const dispatch = useDispatch()

    let storage = window.localStorage;

    useEffect(() => {
        storage.auth
            ? handleUserInfo()
            : dispatch(setLogin({ loggedIn: false }))
    })

    const handleUserInfo = () => {
        dispatch(setLogin({ loggedIn: true }))
        handleUserRole();
    }

    const handleUserRole = () => {
        return userDetails().then(res => {
            const role = res.data.data.role;
            dispatch(setRole({ role }))
        }).catch(err => console.log(err))
    }

    const renderLogin = () => {
        if (status === true) {
            return <LogoutButton />
        }

        return (
            <div>
                <LoginForm />
            </div>
        )
    }

    return (
        <div>
            {renderLogin()}
        </div>
    )
}

export default AuthContainer;
