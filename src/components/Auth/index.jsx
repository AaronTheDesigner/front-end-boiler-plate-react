import React from 'react';
import AuthContainer from './AuthContainer';

const index = () => {
    return (
        <div>
            <AuthContainer />
        </div>
    )
}

export default index;
