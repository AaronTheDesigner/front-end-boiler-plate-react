import React from "react";
import { useStateMachine } from "little-state-machine";
import { useForm } from "react-hook-form";
import { withRouter } from "react-router-dom";
import updateAction from "./updateAction";
import { createArticle } from '../../axios/homeApi';

import FullCard from '../../components/FullCard';

const Confirm = props => {
    const { handleSubmit } = useForm();
    const { action, state } = useStateMachine(updateAction);

    const onSubmit = () => {
        const data = state.data;
        createArticle(data)
        .then(res => {            
            const post = res.data.data;
            console.log(post)
            action(post)
        })
        .catch(err => console.log(err))

        props.history.push('./picture')
    }

    const createMarkup = () => {
        return {__html: state.data.content}
    }

    const set = () => {
        return <div dangerouslySetInnerHTML={createMarkup()} />
    }

    return (    
            <div>
                <FullCard 
                    title={state.data.title} 
                    subtitle={state.data.subtitle} 
                    preview={state.data.description}
                    createdAt={state.data.createdAt}
                    photo={state.data.photo}
                />
                {set()}
                <form onSubmit={handleSubmit(onSubmit)} >
                    <input type="submit" value="publish"/>    
                </form>           
            </div>
    );
};

export default withRouter(Confirm);
