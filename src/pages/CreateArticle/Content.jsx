import React, { useState } from 'react';
import { useForm } from "react-hook-form";
import { withRouter } from "react-router-dom";
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useStateMachine } from "little-state-machine";
import updateAction from "./updateAction";

const Content = (props) => {
    const { handleSubmit } = useForm();
    const [ckData, setCkData] = useState(null);
    const { action, state } = useStateMachine(updateAction);

    const onSubmit = (data) => {
        modifyContent(data)
        action(data)
        props.history.push('./confirm')
    }

    const modifyContent = (data) => {
        // React Hook Form troubles with CK and handleSubmit
        // manually replace content with ckData
        data.content = ckData
    }

    const goBack = () => {
        props.history.push('./headers')
    }

    return (
        <div>
            <button onClick={goBack} >Back</button>
            <form onSubmit={handleSubmit(onSubmit)}>
                <CKEditor
                        data={state.data.content}
                        editor={ClassicEditor}
                        onBlur={(event, editor) => {
                            const content = editor.getData();
                            setCkData(content)
                        }}
                        onChange={ ( event, editor ) => {
                            const data = editor.getData();
                            setCkData(data)
                        } }
                    />
                <input type="submit" value="submit"/>
            </form>
        </div>
    )
}

export default withRouter(Content);
