import React from 'react';
import { BrowserRouter as Router, Route, useRouteMatch } from 'react-router-dom';
import { StateMachineProvider, createStore } from 'little-state-machine';
import Start from './Start';
import Headers from './Headers';
import Content from './Content';
import Picture from './Picture';
import Confirm from './Confirm';
import Blog from '../Blog';

createStore({
    data: {}
});

const index = () => {

    return (
        <StateMachineProvider>
            <h1>Article</h1>

            <Router>
                <Route path="/" component={Start} />
                <Route exact path="/headers" component={Headers} />
                <Route path="/content" component={Content} />
                <Route path="/confirm" component={Confirm} />
                <Route path="/picture" component={Picture} />
                <Route path="/blog" component={Blog} />
            </Router>

        </StateMachineProvider>
    )

}

export default index;