import React, { useState } from 'react';

const ArticleNav = (props) => {

    const {
        view: [view, setView]
    } = {
        view: useState(''),
        ...(props.state || {})
    }

    return (
        <div>
            <h3>{view}</h3>
            <button onClick={() => setView('headers')} >Headers</button>
            <button onClick={() => setView('content')} >Content</button>
            <button onClick={() => setView('picture')} >Picture</button>
            <button onClick={() => setView('preview')} >Preview</button>
        </div>
    )
}

export default ArticleNav
