import React, { useState } from 'react';
import { useStateMachine } from "little-state-machine";
import { withRouter } from "react-router-dom";
import updateAction from "./updateAction";
import axios from 'axios';

const Picture = (props) => {
    const { state } = useStateMachine(updateAction);
    const [file, setFile] = useState(null);
    const [filename, setFilename] = useState(null);

    const data = state.data;

    const onSubmit = (e) => {
        e.preventDefault();
        let formData = new FormData();
        formData.append('file', file);
        formData.append('name', filename);
        formData.append('type', file.type);
        
        axios.put(`/articles/${ state.data.id }/photo`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            props.history.push(`/`);
        }).catch(err => console.log(err.response.data.error))
    }

    const handleChange = (e) => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);
    }

    const clearData = () => {            
        Object.keys(data).forEach(key => data[key]=null)
        console.log(data)
    }


    return (
        <form onSubmit={onSubmit}>
            <label>
                Select A Cover Image:
            </label>
            <input 
                type="file" 
                name="file" 
                onChange={handleChange}
            />
            <input type="submit" value="submit" accept="image/png, image/jpeg, image/svg" />
        </form>
    )
}

export default withRouter(Picture);
