import React, { useEffect } from 'react'
import { useForm } from "react-hook-form";
import { withRouter } from "react-router-dom";
import { useStateMachine } from "little-state-machine";
import updateAction from "./updateAction";

const Start = (props) => {
    const { register, handleSubmit } = useForm();
    const { action, state } = useStateMachine(updateAction);
    let data = state.data
    useEffect(() => {
        
        const clearData = () => {            
            Object.keys(data).forEach(key => data[key]=null)
            console.log(data)
        }
        clearData();
    }, [])
    
    const onSubmit = data => {
        action(data);
        props.history.push("./headers");
    };
    
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <label >Public</label>
            <input type="checkbox" placeholder="public" name="pub" ref={register} />

            <input type="submit" value="next" />

        </form>

    )
}

export default withRouter(Start);
