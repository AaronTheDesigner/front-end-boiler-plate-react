import React from 'react';
import Content from '../Content';
import Headers from '../Headers';
import Picture from '../Picture';
import Preview from '../Preview'

const ArticleContainer = (props) => {

    const renderView = () => {
        return  props.view === 'content' ? <Content />
        : props.view === 'picture' ? <Picture />
        : props.view === 'preview' ? <Preview />
        : <Headers />
    }

    return (
        <div>
            {renderView()}
        </div>
    )
}

export default ArticleContainer
