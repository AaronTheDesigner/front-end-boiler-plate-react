import React from 'react'
import { useForm } from "react-hook-form";
import { withRouter } from "react-router-dom";
import { useStateMachine } from "little-state-machine";
import updateAction from "./updateAction";

const Headers = (props) => {
    const { register, handleSubmit } = useForm();
    const { action, state } = useStateMachine(updateAction);
    const onSubmit = data => {
        
        modifyTag(data)
        action(data);
        props.history.push("./content");
    };

    const modifyTag = data => {
        let tagArray = [];
        tagArray.push(data.tagOne, data.tagTwo)
        data.tag = tagArray;
        delete data.tagOne;
        delete data.tagTwo;
    }

    
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <input type="text" name="title" placeholder="Title" defaultValue={state.data.title} ref={register} />
            <input type="text" name="subtitle" placeholder="Subtitle" defaultValue={state.data.subtitle} ref={register}  />
            <textarea name="description" placeholder="Description: for previews and social media." cols="30" rows="15" defaultValue={state.data.description} ref={register} />
            <select name="tagOne" defaultValue={state.data.tagOne} ref={register}>
                    <option value="Web Development">Web Development</option>
                    <option value="Mobile Development"> Mobile Development</option>
                    <option value="Tutorial"> Tutorial</option>
                    <option value="Diary"> Diary</option>
                    <option value="Web Design"> Web Design</option>
                    <option value="UI/UX"> UI/UX</option>
                </select>
            <select name="tagTwo" defaultValue={state.data.tagTwo} ref={register}>
                    <option value="Web Development">Web Development</option>
                    <option value="Mobile Development"> Mobile Development</option>
                    <option value="Tutorial"> Tutorial</option>
                    <option value="Diary"> Diary</option>
                    <option value="Web Design"> Web Design</option>
                    <option value="UI/UX"> UI/UX</option>
            </select>
            <input type="submit" value="submit"/>
        </form>

    )
}

export default withRouter(Headers);
