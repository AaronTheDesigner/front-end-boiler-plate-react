import React, { useEffect, useState } from 'react';
import { getProjects } from '../../axios/homeApi';
import Gallery from './Gallery';


const ProjectsContainer = () => {
    const [projects, setProjects] = useState([]);

    useEffect(() => {
        fetchProjects();
    }, [])

    const fetchProjects = () => {
        getProjects().then(res => {
            const projects = res.data.data;
            setProjects(projects)
        })
    }


    return (
        <div>
            <Gallery projects={projects} />
        </div>
    )
}

export default ProjectsContainer
