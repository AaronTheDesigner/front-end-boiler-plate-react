import React from 'react';
import ProjectsContainer from './ProjectsContainer';

const index = () => {
    return (
        <div>
           <ProjectsContainer />
        </div>
    )
}

export default index
