import React from 'react';
import LargeCard from '../../components/LargeCard';

const Gallery = (props) => {
    
    const renderGallery = () => {
        return props.projects.map(project => {
            return (
                <LargeCard 
                    key={project.id}
                    photo={project.image}
                    name={project.name}
                    intro={project.intro}
                    description={project.description}
                    status={project.status}
                    languages={project.languages}
                    technologies={project.technologies}
                    client={project.client}
                />
            )
        })
    }

    return (
        <div>
            {renderGallery()}
        </div>
    )
}

export default Gallery
