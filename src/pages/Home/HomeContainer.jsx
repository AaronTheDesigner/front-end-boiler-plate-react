import React, { useEffect, useState } from 'react';
import { getHomepages } from '../../axios/homeApi';

const HomeContainer = () => {
    const [homepage, setHomepage] = useState([]);

    useEffect(() => {
        fetchHomepage();
    }, [])

    const fetchHomepage = () => {
        return getHomepages()
        .then(res => {
            const homepage = res.data.data[0];
            setHomepage(homepage)
        })
    }

    return (
        <div>
            <h1>Header</h1>
            <p>{homepage.header}</p>
            <h1>About</h1>
            <p>{homepage.about}</p>
            <h1>Mid</h1>
            <p>{homepage.mid}</p>
            <h1>Low</h1>
            <p>{homepage.low}</p>
            <h1>Contact</h1>
            <p>{homepage.contact}</p>
            <h1>Footer</h1>
            <p>{homepage.footer}</p>
        </div>
    )
}

export default HomeContainer
