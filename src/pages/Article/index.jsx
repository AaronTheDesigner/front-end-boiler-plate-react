import React from 'react'
import ArticleContainer from './ArticleContainer';

const index = (props) => {
    return (
        <div>
           <ArticleContainer id={props.location.state.id}/>
        </div>
    )
}

export default index
