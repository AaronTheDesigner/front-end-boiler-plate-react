import React, { useState, useEffect } from 'react';
import FullCard from '../../components/FullCard';
import UserCard from '../../components/UserCard';
import { getArticle, getUser } from '../../axios/homeApi';

const ArticleContainer = (props) => {
    const [article, setArticle] = useState({});
    const [author, setAuthor] = useState({});
    const id = props.id
    
    useEffect (() => {
        const fetchArticle = () => {
            return getArticle(id)
                    .then(res => {
                        const article = res.data.data;
                        setArticle(article);
                        fetchAuthor(article.user);
                    });
        };

         fetchArticle();
    }, [id]);

    const fetchAuthor = (user) => {
        return getUser(user)
        .then(res => {
            const author = res.data.data;
            setAuthor(author);
        })
    }

    return (
        <div>
            <UserCard 
                username={author.username}
                firstname={author.firstname}
                lastname={author.lastname}
                profession={author.profession}
                company={author.company}
                email={author.email}
                photo={author.image}
            />
            <FullCard 
                title={article.title} 
                subtitle={article.subtitle} 
                preview={article.description}
                content={article.content}
                createdAt={article.createdAt}
                photo={article.photo}
            />            
        </div>
    )
}

export default ArticleContainer
