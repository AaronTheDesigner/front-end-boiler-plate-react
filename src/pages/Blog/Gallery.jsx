import React from 'react';
import MediumCard from '../../components/MediumCard';
import { Link } from 'react-router-dom';

const Gallery = (props) => {

    const renderGallery = () => {    
    return props.articles.map(article => {
            return (
                <Link key={article.id} to={{
                    pathname: `/blog/${ article.slug }`,
                    state: { id: article.id }
                }} >
                    <MediumCard
                        title={article.title}
                        subtitle={article.subtitle}
                        preview={article.preview}
                        created={article.createdAt}
                        photo={article.photo}
                    />
                </Link>
            )
        })
    }
    
    return (
        <div>
            {renderGallery()}
        </div>
    )
}

export default Gallery
