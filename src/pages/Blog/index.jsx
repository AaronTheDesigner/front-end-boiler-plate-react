import React from 'react'
import BlogContainer from './BlogContainer'

const index = () => {
    return (
        <div>
           <h1>Blog</h1> 
           <BlogContainer />
        </div>
    )
}

export default index
