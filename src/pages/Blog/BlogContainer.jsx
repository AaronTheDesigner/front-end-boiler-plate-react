import React, { useEffect, useState } from 'react';
import { getArticles } from '../../axios/homeApi';
import Gallery from './Gallery';

const Blog = () => {
    const [articles, setArticles] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        fetchArticles();        
    }, [])

    const fetchArticles = () => {
        return getArticles().then(res => {
            const articles = res.data.data;
            setArticles(articles)
            setLoading(false)
        })
    }

    return (
        <div>
            <Gallery loading={loading} articles={articles}/>
        </div>
    )
}

export default Blog
