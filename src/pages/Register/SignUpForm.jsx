import React from 'react';
import { useForm } from 'react-hook-form';
import { signUp } from '../../axios/homeApi';

const SignUpForm = () => {
    const { handleSubmit, register, errors } = useForm();

    const onSubmit = data => {
        console.log(data)
        signUp(
            data.name, 
            data.email, 
            data.password,
            data.newsletter
            )
            .then(res => {
                console.log(res)
            })
            .catch(err => {
                console.log(err.response.data.error)
            })
    }
// okay   

    return (
        <form onSubmit={handleSubmit(onSubmit)} >
            <input 
                type="text" 
                name="name"
                placeholder="Username"
                ref={register({
                    required: true,
                    maxLength: 30
                })}
            />
            <input 
                type="email" 
                name="email" 
                placeholder="Email"
                ref={register({
                    required: true
                })}
            />
            <input 
                type="password" 
                name="password" 
                placeholder="Password"
                ref={register({
                    required: true,
                    minLength: 6
                })}
            />
            <p>Would you like to sign up for our newsletter.</p>
            <input 
                type="checkbox" 
                placeholder="Newsletter" 
                name="Newsletter" 
                ref={register} 
            />
            <br/>
            <input type="submit" value="Submit"/>
        </form>
    )
}

export default SignUpForm
