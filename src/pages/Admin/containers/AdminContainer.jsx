import React from 'react'
import AnalyticsContainer from './AnalyticsContainer';
import ArticlesContainer from './ArticlesContianer';
import HomepageContainer from './HomepageContainer';
import UsersContainer from './UsersContainer';
import CreateArticle from '../../CreateArticle';

const AdminContainer = (props) => {

    const renderView = () => {
        return props.view === 'articles' ? <ArticlesContainer />
            : props.view === 'users' ? <UsersContainer />
                : props.view === 'create' ? <CreateArticle />
                    : props.view === 'home' ? <HomepageContainer />
                        : <AnalyticsContainer />
    }

    return (
        <div>
            {renderView()}
        </div>
    )
}

export default AdminContainer
