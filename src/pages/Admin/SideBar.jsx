import React, { useState } from 'react';

const SideBar = (props) => {

    const {
        view: [view, setView]
    } = {
        view: useState(''),
        ...(props.state || {})
    }

    return (
        <div>
            <h3>{view}</h3>
            <button onClick={() => setView('main')} >Main</button>
            <button onClick={() => setView('users')} >Users</button>
            <button onClick={() => setView('articles')} >Articles</button>
            <button onClick={() => setView('create')} >Publish</button>
            <button onClick={() => setView('home')} >Homepage</button>
        </div>
    )
}

export default SideBar