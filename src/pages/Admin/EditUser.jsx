import React, {useState, useEffect} from 'react';
import {useForm} from 'react-hook-form';
import { getUser, updateUser, deleteUser } from '../../axios/homeApi';

const EditUser = (props) => {
    const [userId] = useState(props.id);
    const {control, handleSubmit, register} = useForm();
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [role, setRole] = useState('');
    const [warning, setWarning] = useState('');

    const onSubmit = data => {
        updateUser(
            userId,
            data.name,
            data.email,
            data.role
        ).then(res => console.log(res.data.data))
            .catch(err => console.log(err.response.data.error))
    }

    useEffect(() => {
        fetchUser(userId)
    }, [])

    const fetchUser = (id) => {
        const fetch = getUser(id).then(res => {
            const user = res.data.data;
            const name = user.name;
            const email = user.email;
            const role = user.role;
            setName(name)
            setEmail(email)
            setRole(role)
        })

        return fetch
    }

    const removeUser = () => {
        deleteUser(userId).then(res => console.log(res))
    }

    const renderWarning = () => {
        if (warning !== '') {
            return (
                <div>
                    {warning}
                    <input type="submit" value="no" onClick={() => setWarning('')}/>
                    <input type="submit" value="yes" onClick={() => removeUser()}/>
                </div>
            )
        }
    }

    return (
        <div>
            Edit {name}
            <form onSubmit={handleSubmit(onSubmit)} >
                <input type="text" name="name" onChange={e => (e.target.value)} value={name || ``} ref={register} />
                <input type="text" name="email" onChange={e => (e.target.value)} value={email} ref={register} />
                <select name="role" value={role} onChange={e => (e.target.value)} ref={register}>
                    <option value="user">User</option>
                    <option value="contributor">Contributor</option>
                    <option value="admin">Administrator</option>
                </select>
            </form>
            <input type="submit" value="back" onClick={() => {props.view('')}} />
            <input type="submit" value="delete" onClick={() => setWarning("Are you sure?")} />
            {renderWarning()}
        </div>
        
    )
}

export default EditUser
