import React, { useState, useEffect } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { getArticle, updateArticle, deleteArticle } from '../../axios/homeApi';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';



const EditArticle = (props) => {
    const [articleId] = useState(props.id);
    const { control, handleSubmit, register } = useForm();
    const [ckData, setCkdata] = useState(null);
    const [subtitle, setSubtitle] = useState('');
    const [description, setDescription] = useState('');
    const [content, setContent] = useState('');
    const [tag_one, setTag_one] = useState('');
    const [tag_two, setTag_two] = useState('');
    const [pub, setPub] = useState(Boolean);
    const [warning, setWarning] = useState('');  

    const onSubmit = data => {
        modifyTag(data)
        modifyContent(data)
        updateArticle(
            articleId,
            data.subtitle,
            data.desctiption,
            data.content,
            data.tag,
            data.pub
        ).then(res => console.log(res.data.data))
            .catch(err => console.log(err.response.data.error))
    }

    const modifyContent = data => {

        return data.content !== '' ? data.content = content
            : data.content = ckData

    }

    const modifyTag = data => {
        let tagArray = [];
        tagArray.push(data.tagOne, data.tagTwo)
        data.tag = tagArray;
        delete data.tagOne;
        delete data.tagTwo;
    }

    const removeArticle = () => {
        deleteArticle(articleId).then(res => {console.log(res)})
    }

    const renderWarning = () => {
        if (warning !== '') {
           return ( <div>
                {warning}
                <input type="submit" value="no" onClick={() => setWarning('')}/>
                <input type="submit" value="yes" onClick={() => removeArticle()}/>
            </div>
           )
        }

    }

    useEffect(() => {
        fetchArticle(articleId)
    }, [articleId])

    const fetchArticle = (id) => {
        const fetch = getArticle(id).then(res => {
            const article = res.data.data;
            const subtitle = article.subtitle;
            const description = article.description;
            const content = article.content;
            const tag_one = article.tag[0];
            const tag_two = article.tag[1];
            const pub = article.public;
            setSubtitle(subtitle);
            setDescription(description);
            setContent(content);
            setTag_one(tag_one);
            setTag_two(tag_two);
            setPub(pub);

        })
        return fetch;
    }

    return (
        <div>

            <form onSubmit={handleSubmit(onSubmit)} >
                <input type="text" name="subtitle" onChange={e => setSubtitle(e.target.value)} placeholder="Subtitle" value={subtitle || ``} ref={register} />
                <textarea name="description" cols="30" rows="15" onChange={e => setDescription(e.target.value)} value={description} ref={register} />
                <Controller
                    as={<CKEditor
                        data={content}
                        editor={ClassicEditor}
                        onBlur={(event, editor) => {
                            const content = editor.getData();
                            setCkdata(content)
                        }}
                    />}
                    name="content" control={control}
                />
                <select name="tagOne" value={tag_one} onChange={e => setTag_one(e.target.value)} ref={register}>
                    <option value="Web Development">Web Development</option>
                    <option value="Mobile Development"> Mobile Development</option>
                    <option value="Tutorial"> Tutorial</option>
                    <option value="Diary"> Diary</option>
                    <option value="Web Design"> Web Design</option>
                    <option value="UI/UX"> UI/UX</option>
                </select>
                <select name="tagTwo" value={tag_two} onChange={e => setTag_two(e.target.value)} ref={register}>
                    <option value="Web Development">Web Development</option>
                    <option value="Mobile Development"> Mobile Development</option>
                    <option value="Tutorial"> Tutorial</option>
                    <option value="Diary"> Diary</option>
                    <option value="Web Design"> Web Design</option>
                    <option value="UI/UX"> UI/UX</option>
                </select>
                <select name="pub" value={pub} onChange={e => setPub(e.target.value)} ref={register}>
                    <option value={true}>true</option>
                    <option value={false}>false</option>
                </select>
                <input type="submit" />
            </form>
            <input type="submit" value="back" onClick={() => {props.view('')}} />
            <input type="submit" value="delete" onClick={() => setWarning("Are you sure?")} />
             {renderWarning()}           

        </div>
    )
}

export default EditArticle
