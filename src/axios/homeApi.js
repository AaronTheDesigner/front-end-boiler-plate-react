import Axios from 'axios';

Axios.defaults.baseURL = 'https://toliver.app/api/v1';
//Axios.defaults.baseURL = 'http://localhost:5000/api/v1';
Axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('auth');
Axios.defaults.headers.post['Content-Type'] = 'application/json';

export const url = 'https://toliver.app';
//export const url = 'http://localhost:5000/api/v1';
// AUTH \\
//public\\
export const signUp = (username, email, password, newsletter) => {
    return Axios.post('/auth/register', {
        username,
        email,
        password,
        newsletter
    })
}

export const login = (email, password) => {
    return Axios.post(`/auth/login`, {
        email,
        password
    })
}

//private\\

export const userDetails = () => {
    return Axios.get('auth/me')
}

export const logout = () => {
    return Axios.get('/auth/logout')
}

// USERS \\
//public\\
export const getUsers = () => {
    return Axios.get('/users')
}

//private\\
export const getUser = (id) => {
    return Axios.get(`/users/${ id }`)
}


export const createUser = (name, email, password, role) => {
    return Axios.post(`/users`, {
        name,
        email,
        password,
        role
    })
}

export const deleteUser = (id) => {
    return Axios.delete(`/users/${id}`)
}

export const updateUser = (id, name, email, role) => {
    return Axios.put(`/users/${id}`, {
        name,
        email,
        role
    })
}


// ARTICLES \\
//public\\

export const getArticles = () => {
    return Axios.get('articles')
}

export const getArticlePhoto = (id) => Axios.create({
    url: `/uploads/photo_${ id }.jpg`,
    method: 'get'

})

export const getArticle = (id) => {
    return Axios.get(`/articles/${ id }`)
}

//private\\
export const createArticle = ({ title, subtitle, description, content, tag, pub }) => {
    return Axios.post('/articles', {
        title,
        subtitle,
        description,
        content,
        tag,
        pub
    })
}

export const uploadPhoto = (id, file) => {
    return Axios.put(`/articles/${ id }/photo`, {
        file
    })
}

export const updateArticle = (id, subtitle, description, content, tag, pub) => {
    return Axios.put(`/articles/${ id }`, {
        subtitle,
        description,
        content,
        tag,
        pub
    })
}


export const deleteArticle = (id) => {
    return Axios.delete(`/articles/${ id }`)
}

// HOMEPAGE \\
// public \\
export const getHomepage = (id) => {
    return Axios.get(`/homepage/${ id }`)
}

export const getHomepages = () => {
    return Axios.get(`/homepage`)
}

export const updateHomepage = (id, header, about, mid, low, contact, footer) => {
    return Axios.put(`/homepage/${ id }`, {
        header,
        about,
        mid,
        low,
        contact,
        footer
    })
}

// PROJECTS \\
// Project \\
export const getProject = (id) => {
    return Axios.get(`/projects/${ id }`)
}

export const getProjects = () => {
    return Axios.get(`/projects`)
}