import { SET_LOGIN } from '../actions/actionTypes';

const initialState = {
    loggedIn: false
}

export const status = (state = initialState, action) => {
    switch (action.type) {
        case SET_LOGIN:
            return {
                ...state,
                ...state.loggedIn = action.bool
            }
        default:
            return state
    }
}