import { combineReducers } from 'redux';
import { permissions } from './permissions';
import { status } from './status';

export default combineReducers({
    permissions,
    status
})