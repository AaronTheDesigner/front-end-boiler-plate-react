import { SET_ROLE, SET_LOGIN, SET_TITLE } from './actionTypes';

export const setRole = (str) => {
    return { type: SET_ROLE, str }
}

export const setLogin = (bool) => {
    return { type: SET_LOGIN, bool }
}

export const setTitle = (str) => {
    return { type: SET_TITLE, str }
}