import React from 'react';
import './App.css';
//Navigation\\
import Nav from './components/Nav';
//Top Routes\\
import Home from './pages/Home';
import Blog from './pages/Blog';
import Projects from './pages/Projects';
import Register from './pages/Register';
//Customer Routes\\
import Tracker from './pages/Tracker';
import Profile from './pages/Profile';
//Contributor Routes\\
import Dash from './pages/Dash';
//Administrator Routes
import Admin from './pages/Admin';
//Public Pages\\
import Article from './pages/Article';
import Recovery from './pages/Recovery';

//Router\\
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

function App() {
  return (
    <div>
      <Router>
        <Nav />
        <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/blog' exact component={Blog} />
            <Route path='/blog/:slug' component={Article} />
            <Route path='/projects' component={Projects} />
            <Route path='/tracker' component={Tracker} />
            <Route path='/profile' component={Profile} />
            <Route path='/dash' component={Dash} />
            <Route path='/admin' component={Admin} />
            <Route path='/register' component={Register} /> 
            <Route path='/recovery' component={Recovery} />           
        </Switch>
      </Router>
    </div>
  );
}

export default App;
